package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    private TextField usernameTextfile;
    @FXML
    private PasswordField passwordField;
    private SampleAutorize auth;
    public  LoginController(){
        auth = new SampleAutorize();
    }
    public void handleLogin(){
        if(auth.isAutorizen(usernameTextfile.getText(), passwordField.getText())){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Autorized");
            alert.setContentText("Succesful");
            alert.setHeaderText("Welcome");
            alert.show();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Not autorized");
            alert.setContentText("Failure");
            alert.setHeaderText("-");
            alert.show();

        }
    }
}
